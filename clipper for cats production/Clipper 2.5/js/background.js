ANTClipper = {
	Settings: {
		baseUrl: "http://cats-sdna.rhcloud.com"
	}
},
String.prototype.host = function () {
	var e = document.createElement("a");
	return e.href = this,
	e.hostname
},
ANTClipper.Main = function () {
	function signed(e) {
		e ? window.localStorage.setItem("ant-signed-in", e) : window.localStorage.removeItem("ant-signed-in")
	}
	function get_signed() {
		return window.localStorage.getItem("ant-signed-in")
	}
	function n(e, o, n) {
		var t = "data (" + e + ")",
		r = window.localStorage.getItem(t),
		s = l + "&act=lead&content=" + o;
		//s = l + "/index.php?m=candidates";
		if (r) {
			var i = !1,
			a = 0;
			for (r = $.parseJSON(r), a = 0; r.length > a; a++) if (r[a].organization_id === n) {
				r[a].lead_id = o,
				r[a].lead_url = s,
				i = !0;
				break
			}
			i || r.push({
				//organization_id: n,
				lead_id: o,
				lead_url: s
			})
		} else r = [{
			//organization_id: n,
			lead_id: o,
			lead_url: s
		}];
		window.localStorage.setItem(t, JSON.stringify(r))
	}
	function t(e) {
		var o = "data (" + e + ")",
		n = window.localStorage.getItem(o);
		return n ? $.parseJSON(n) : []
	}
	function login(o) {
		$.ajax({
			type: "POST",
			url: l + "/api.php?vers=v1&act=login",
			data: {
				username: o.username,
				password: o.password
			},
			dataType: "json",
			success: function (data) {
				
				if(data.error)
				{
					signed(!1),
					o.success(data)
				}
				else
				{
					chrome.cookies.get({
						url: l,
						name: "auth"
					},
					function (e) {
						if(e != null)
						{
							authToken = e.value
						}
					}),
					acc_info({
						success: o.success,
						error: o.error
					})
				}
			},
			error: function (n) {
				alert(username),
				signed(!1),
				o.error(n)
			}
		})
	}
	function logout(e) {
		var o = function () {
			$.ajax({
				type: "POST",
				url: l + "/api.php?vers=v1&act=logout",
				success: function (o) {
					e.success(o)
				}
			})
		};
		o();		
		authToken ? get_signed() : chrome.cookies.get({
			url: l,
			name: "auth"
		},
		function (e) {
			if(e != null)
			{
				authToken = e.value,
				get_signed()
			}
		})		
	}
	function acc_info(o) {
		$.ajax({
			url: l + "/api.php?vers=v1&act=me",
			dataType: "json",
			success: function (n) {
				if(n.error)
				{
					signed(!1),
					o.error(n)
				}
				else
				{
					signed(!0),
					save_info(n),
					o.success(n)
				}
			},
			error: function (n) {
				signed(!1),
				o.error(n)
			}
		})
	}
	function save_info(e) {
		e ? localStorage.setItem("ant-me", JSON.stringify(e)) : localStorage.removeItem("ant-me")
	}
	function user_detail() {
		var e = $.parseJSON(localStorage.getItem("ant-me"));
		return ! e.currentOrg && e.organizations && (e.currentOrg = e.organizations[0]),
		e
	}
	function send_info(e) {
		var o = e.lead;
		//o.organization_id = user_detail().currentOrg.id;
		var n = function () {
			$.ajax({
				url: l + "/api.php?vers=v1&act=lead",
				type: "POST",
				data: JSON.stringify(o),
				contentType: "application/json",
				dataType: "json",
				success: function (o) {
					e.success(o)
				},
				error: function (o) {
					console.log(o),
					e.error(o)
				}
			})
		};
		authToken ? n() : chrome.cookies.get({
			url: l,
			name: "auth"
		},
		function (e) {
			if(e != null)
			{
				authToken = e.value,
				n()
			}
		})
	}
	function check_user(e) {
		var o = e.lead;
		var n = function () {
			$.ajax({
				url: l + "/api.php?vers=v1&act=check_lead",
				type: "POST",
				data: JSON.stringify(o),
				contentType: "application/json",
				dataType: "json",
				success: function (o) {
					e.success(o)
				},
				error: function (o) {
					console.log(o),
					e.error(o)
				}
			})
		};
		authToken ? n() : chrome.cookies.get({
			url: l,
			name: "auth"
		},
		function (e) {
			if(e != null)
			{
				authToken = e.value,
				n()
			}
		})
	}
	var l = ANTClipper.Settings.baseUrl;
	return authToken = null,
	{
		init: function () {
			chrome.webRequest.onBeforeSendHeaders.addListener(function (e) {
				for (var o = !1, n = 0; e.requestHeaders.length > n; ++n) if ("Referer" === e.requestHeaders[n].name) {
					o = !0,
					e.requestHeaders[n].value = "http://cats-sdna.rhcloud.com/";
					break
				}
				return o ? {
					requestHeaders: e.requestHeaders
				}: (e.requestHeaders.push({
					name: "Referer",
					value: "https://cats-sdna.rhcloud.com/"
				}), {
					requestHeaders: e.requestHeaders
				})
			},
			{
				urls: ["<all_urls>"]
			},
			["requestHeaders", "blocking"]),
			chrome.cookies.onChanged.addListener(function (e) {
				"explicit" !== e.cause || "session" !== e.cookie.name && "auth" !== e.cookie.name || l.host() !== e.cookie.domain || e.cookie.value !== localStorage.getItem("ant-" + e.cookie.name + "-cookie") && (localStorage.setItem("ant-" + e.cookie.name + "-cookie", e.cookie.value), acc_info({
					success: function () {
						chrome.cookies.get({
							url: l,
							name: "auth"
						},
						function (e) {
							authToken = e.value
						}),
						chrome.browserAction.setPopup({
							popup: "templates/menu.html"
						})
					},
					error: function () {
						chrome.browserAction.setPopup({
							popup: "templates/sign_in.html"
						})
					}
				}))
			}),
			get_signed() ? chrome.browserAction.setPopup({
				popup: "templates/menu.html"
			}) : chrome.browserAction.setPopup({
				popup: "templates/sign_in.html"
			}),
			acc_info({
				success: function () {
					chrome.browserAction.setPopup({
						popup: "templates/menu.html"
					})
				},
				error: function () {
					chrome.browserAction.setPopup({
						popup: "templates/sign_in.html"
					})
				}
			}),
			chrome.extension.onRequest.addListener(function (o, i, p) {
				if ("login" === o.type) login({
					username: o.username,
					password: o.password,
					success: function (e) {
						p({
							success: !e.error,
							message: e.message
						})
					},
					error: function (e) {
						p({
							statusCode: e.status,
							message: e.statusText
						})
					}
				});
				else if ("logout" === o.type) logout({
					success: function (e) {
						signed(!1),
						save_info(!1),
						chrome.browserAction.setPopup({
							popup: "templates/sign_in.html"
						}),
						p({
							success: !0
						})
					}
				});
				else if ("loadMenu" === o.type) chrome.browserAction.setPopup({
					popup: "templates/menu.html"
				}),
				p({
					success: !0
				});
				else if ("getUserDetails" === o.type) {
					var d = user_detail();
					p({
						data: d
					})
				} else if ("changeOrg" === o.type) {
					var m = user_detail();
					$.each(m.organizations, function (e) {
						m.organizations[e].id === o.id && (m.currentOrg = m.organizations[e])
					}),
					save_info(m),
					p(m)
				} else if ("postLead" === o.type) {
					var f = o.lead,
					g = f.custom.original_url;
					send_info({
						lead: f,
						success: function (e) {
							n(g, e.id, e),
							p({
								success: !e.error,
								message: e.message,
								data: e,
								url: l + "/index.php?m=candidates&a=show&candidateID=" + e.id
							})
						},
						error: function (e) {
							p({
								statusCode: e.status,
								message: e.statusText,
								xhr: e
							})
						}
					})
				} else if ("check_lead_exists" === o.type) {
					var f = o.lead,
					g = f.custom.original_url;
					check_user({
						lead: f,
						success: function (e) {
							n(g, e.id, e),
							p({
								success: !e.error,
								message: e.message,
								data: e,
								url: l + "/index.php?m=candidates&a=show&candidateID=" + e.id
							})
						},
						error: function (e) {
							p({
							    statusCode: e.status,
							    message: e.statusText,
							    xhr: e
							})
						}
					})
				} else "getCurrentLead" === o.type && chrome.windows.getLastFocused(function (e) {
					chrome.tabs.query({
						active: !0,
						windowId: e.id
					},
					function (e) {
						e.length && chrome.tabs.sendRequest(e[0].id, {
							type: "getLead"
						},
						function (o) {
							p({
								lead: o,
								postedData: t(e[0].url)
							})
						})
					})
				})
			}),
			chrome.omnibox.onInputStarted.addListener(function () {
				console.log("Omnibox: input started.")
			}),
			chrome.omnibox.onInputCancelled.addListener(function () {
				console.log("Omnibox: input cancelled.")
			}),
			chrome.omnibox.onInputChanged.addListener(function () {}),
			chrome.omnibox.onInputEntered.addListener(function () {})
		}
	}
} (),
$(document).ready(function () {
	ANTClipper.Main.init()
});
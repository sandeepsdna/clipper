function isRFC822ValidEmail(e) {
	var t = "[^\\x0d\\x22\\x5c\\x80-\\xff]",
	r = "[^\\x0d\\x5b-\\x5d\\x80-\\xff]",
	n = "[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+",
	a = "\\x5c[\\x00-\\x7f]",
	i = "\\x5b(" + r + "|" + a + ")*\\x5d",
	s = "\\x22(" + t + "|" + a + ")*\\x22",
	o = n,
	c = "(" + o + "|" + i + ")",
	l = "(" + n + "|" + s + ")",
	d = c + "(\\x2e" + c + ")*",
	u = l + "(\\x2e" + l + ")*",
	p = u + "\\x40" + d,
	m = "^" + p + "$",
	f = RegExp(m);
	return f.test(e) ? !0 : !1
}
function getQueryParam(e, t) {
	t = t.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var r = "[\\?&]" + t + "=([^&#]*)",
	n = RegExp(r),
	a = n.exec(e);
	return null !== a ? decodeURIComponent(a[1].replace(/\+/g, " ")) : !1
}
function handleFacebook() {
	var e = {},
	t = window.location.href;
	if (t.path().match(/\/info$/i)) if ($(".callOut").length) {
		e.name = $(".name").text().split("About")[0].trim();
		var r = $(".callOut .businessAddress ul li");
		e.addresses = [{
			address_1: $(r[0]).text().trim(),
			city: $(r[1]).text().split(",")[0],
			state: $(r[1]).text().split(",")[1].split(" ")[0].trim(),
			zipcode: $(r[1]).text().split(",")[1].split(" ")[1].trim()
		}],
		e.contacts = [{}],
		$(".callOut .contactInfoTable tr").each(function () {
			var t = $(this).find("td:first-child").text().trim(),
			r = $(this).find("td:last-child").text().trim();
			"Email" === t ? e.contacts[0].emails = [{
				email: r,
				type: "office"
			}] : "Phone" === t ? e.contacts[0].phones = [{
				phone: r,
				type: "office"
			}] : "Website" === t && (e.url = r)
		})
	} else {
		var n = $(".experienceTitle");
		n.length && (e.name = $(n[0]).text().trim()),
		e.contacts = [{
			name: $(".name").text().split("About")[0].trim(),
			emails: [],
			phones: [],
			urls: [{
				url: t.substring(0, t.indexOf("/info")),
				type: "url"
			}]
		}];
		var a = $(".experienceBody");
		a.length && (e.contacts[0].title = $(a[0]).text().trim()),
		$(".contactInfoPhone").length && e.contacts[0].phones.push({
			phone: $(".contactInfoPhone").text().trim(),
			type: "office"
		}),
		$(".label").each(function () {
			var t = $(this).text().trim(),
			r = $(this).next().text().trim();
			if ("Website" === t) e.url = r;
			else if ("Email" === t) e.contacts[0].emails.push({
				email: r,
				type: "office"
			});
			else if ("Address" === t) e.addresses = [{
				city: r.split(",")[0].trim(),
				country: r.split(",")[1].trim()
			}];
			else if ("Location" === t) {
				var n = r.split(",");
				e.addresses = [{
					address_1: n[0] && n[0].trim(),
					city: n[1] && n[1].trim(),
					state: n[2] && n[2].split(" ")[0].trim(),
					zipcode: n[2] && n[2].split(" ")[1] && n[2].split(" ")[1].trim()
				}]
			}
		})
	} else {
		var i = $("a.nameButton").children().text(),
		s = $("span.profileName").text(),
		o = s ? s: i,
		c = $("span.prs").toArray()[0];
		c = c ? c.innerText: "";
		var l = $("div.experienceTitle").toArray()[0];
		l = l === void 0 ? "": l.innerText;
		var d = $("i.sx_ca52a0").parent().parent().children("span.fbProfileBylineLabel").children("a").text(),
		u = $("a.nameButton").attr("href") || $("li#navItem_info > a.item").attr("href");
		"pagelet_timeline_page_actions" == $("div.actionsDropdown").attr("id") && (u = ""),
		l && (e.name = l),
		e.contacts = [{
			name: o ? o: null,
			emails: c ? [{
				email: c,
				type: "office"
			}] : [],
			urls: [{
				url: window.location.href,
				type: "url"
			}]
		}],
		d && (e.addresses = [{
			address_1: d
		}])
	}
	return e
}
function handleLinkedIn() {
	var e = $("p.title").text().trim().split(" at "),
	t = $("span.full-name").text(),
	q = $("p.title").text().trim().split(" - ");
	if (e[1] === undefined)
	{
		if (q[1] === undefined) {
			q = $("#overview-summary-current").find("li").find("a").first().text() || "";
			e[1] = q;
		}
		else
		    e[1] = q[1];
		
	}
	t.indexOf(" ");
	var pic = $("div.profile-picture").find("img").attr("src") || "";
	var r = $("div#phone-view").find("li").first().text().match(/[\+\d\s\-\.\(\)]+/);
	//var r = $("div#phone-view").find("li").first().text().match(/[\+\d\s\-\.\(\)]+/).replace(/[^\+\d\s\-\.]/gi,'');
	r && r.length > 0 && (r = $.trim(r[0]));
	r && r.length > 0 && (r = r.split(/\($/)[0].trim());
	var n, a = $("li.abook-phone").toArray();
	a.length > 0 && (n = a[0].innerText.split(" ")[0]);
	var i = "",
	s = $(".twitter-follow-button")[0];
	if (s && s.src) {
		var o = s.src.match(/screen_name=\w+/);
		o.length > 0 && (twitterhandle = o[0].substr("screen_name=".length))
	}
	var c = $("a[name=overviewsite]").attr("href");
	c = c ? c.slice(20, -13) : "",
	r = r || n || "";
	var l = $("div#email-view").find("a").first().text() || $("li.abook-email").children("a").text() || "",
	d = $("p.description").text() || "",
	u = $("div#website-view").find("a").first().attr("href") || unescape(c) || "";
	u && 0 !== u.indexOf("http") && (u = "http://linkedin.com" + u);
	
	var p = $("span.locality").children().text() || "",
	m = {};
	e[1] && (m.name = e[1]),	
	m.contacts = [{
		name: t ? t: null,
		emails: l ? [{
			email: l,
			type: "office"
		}] : [],
		title: e[0] ? e[0] : null,
		
		phones: r ? [{
			phone: r,
			type: "office"
		}] : [],
		urls: []
	}],
	u && (m.url = u),
	d && (m.description = d),
	p && (m.addresses = [{
		address_1: p
	}]),
	m.custom = {
		source: "LinkedIn"
	};
	//List skill
	m.skills = [];
	//var icount = 0;
	//$('span.endorse-item-name-text').each(function(){
	//  m.skills[icount] = $(this).text();
	//  icount ++;
	//})
	//Display profile pic URL
	m.profile_pic = [];
	m.profile_pic = pic;
	var f = $("div#twitter-view").find("a").first().text() || i || "",
	//h = $("dl.public-profile dd").first().find("span").text() || "hee haw";
	h = $("dl.public-profile").find("span").text() || $("dl.public-profile").find("a").attr("href") || "";
	return $("dd.industry").children().text() || "",
	f && m.contacts[0].urls.push({
		url: "https://twitter.com/" + f,
		type: "url"
	}),
	h && m.contacts[0].urls.push({
		url: h,
		type: "url"
	}),
	m
}
function handleGitHub() {
	var e = {};
	return $("body").hasClass("org-profile") ? e.name = $('span[itemprop="name"]').text() : (e.name = $('dd[itemprop="worksFor"]').text(), e.contacts = [{
		name: $('span[itemprop="name"]').text().trim()
	}], $(".email").length && (e.contacts[0].emails = [{
		email: $(".email").text().trim(),
		type: "office"
	}])),
	e.addresses = [{
		city: $('dd[itemprop="homeLocation"]').text().trim()
	}],
	e.url = $('dd[itemprop="url"]').text(),
	e
}
function handleYelp() {
	var e = getQueryParam($("#bizUrl a").attr("href"), "url"),
	t = {
		name: $("h1").text().trim(),
		url: e,
		addresses: [],
		contacts: [{
			emails: [],
			phones: []
		}],
		custom: {}
	},
	r = $('span[itemprop="streetAddress"]').html();
	r && (r = r.split("<br>"), 1 === r.length ? r = {
		address_1: r[0].trim()
	}: r.length >= 2 && (r = {
		address_1: r[0].trim(),
		address_2: r[1].trim()
	}), r.city = $('span[itemprop="addressLocality"]').text().trim(), r.state = $('span[itemprop="addressRegion"]').text().trim(), r.zipcode = $('span[itemprop="postalCode"]').text().trim(), r.country = $('span[itemprop="addressCountry"]').text().trim(), t.addresses.push(r));
	var n = $('span[itemprop="telephone"]').text().trim();
	return n && (t.contacts[0].phones = [{
		phone: n,
		type: "office"
	}]),
	t
}
function handleCrunchBase() {
	var e, t = $("h1.h1_first").clone();
	t.find(".right").remove(),
	t = t.text().trim();
	var r = {
		name: t,
		contacts: [{
			phones: [],
			emails: [],
			urls: []
		}],
		addresses: [{}],
		custom: {}
	},
	n = $("#col1 table tr");
	for (e = 0; n.length > e; e++) {
		var a = $(n[e]),
		i = a.find(".td_left").text().trim(),
		s = a.find(".td_right").text().trim(),
		o = a.find(".td_right a").attr("href");
		switch (o && (0 === o.indexOf("mailto:") ? o = unescape(o.substring(7)) : 0 !== o.indexOf("http") && (o = "http://" + o)), i) {
		case "Website":
			r.url = o;
			break;
		case "Blog":
			r.contacts[0].urls.push({
				url:
				o,
				type: "url"
			});
			break;
		case "Twitter":
			r.contacts[0].urls.push({
				url:
				o,
				type: "url"
			});
			break;
		case "Phone":
			r.contacts[0].phones = [{
				phone: s,
				type: "office"
			}];
			break;
		case "Email":
			r.contacts[0].emails = [{
				email: o,
				type: "office"
			}];
			break;
		case "Employees":
			r.custom.employees = s;
			break;
		case "Founded":
			r.custom.founded = s;
			break;
		case "Description":
			r.description = s
		}
	}
	var c = $(".col1_office_address");
	if (c.length) {
		c = $(c[0]);
		var l = c.clone();
		l.find("a").remove(),
		l = l.html().split("<br>"),
		1 === l.length ? r.addresses[0].address_1 = l[0].trim() : l.length >= 2 && (r.addresses[0].address_1 = l[0].trim(), r.addresses[0].address_2 = l[1].trim()),
		r.addresses[0].address_1.match(/^[ ,]*$/g) && (r.addresses[0].address_1 = ""),
		r.addresses[0].address_2.match(/^[ ,]*$/g) && (r.addresses[0].address_2 = "");
		var d = c.find("a");
		d.each(function () {
			var e = $(this);
			0 === e.attr("href").indexOf("/maps/city/") ? r.addresses[0].city = e.text().trim() : 0 === e.attr("href").indexOf("/maps/state/") ? r.addresses[0].state = e.text().trim() : 0 === e.attr("href").indexOf("/maps/zip/") ? r.addresses[0].zipcode = e.text().trim() : 0 === e.attr("href").indexOf("/maps/country/") && (r.addresses[0].country = e.text().trim())
		})
	}
	return r
}
function handleGmail() {
	var e, t, r = {},
	n = $(".gE:not(.hI)");
	return n.length && n.each(function () {
		var r = $(this).find(".gD");
		r.attr("email") !== $("#gbi4t").text() && (e = r.text().trim(), t = r.attr("email"))
	}),
	e && t && (r.contacts = [{
		name: e,
		emails: [{
			email: t,
			type: "office"
		}]
	}]),
	r
}
function handleJigsaw() {
	var e = {
		name: $("#companyname").text().trim(),
		addresses: [{
			address_1: $("#address").text().trim(),
			address_2: $("#address2").text().trim(),
			city: $("#city").text().trim(),
			state: $("#state").text().trim(),
			zipcode: $("#zip").text().trim(),
			country: $("#country").text().trim()
		}],
		contacts: [{
			name: ($("#firstname").text().trim() + " " + $("#lastname").text().trim()).trim(),
			title: $("#title").text().trim()
		}]
	};
	return $("#phone").length && (e.contacts[0].phones = [{
		phone: $("#phone").text().trim(),
		type: "office"
	}]),
	$("#email").length && (e.contacts[0].emails = [{
		email: $("#email").text().trim(),
		type: "office"
	}]),
	e
}
function handleQuora() {
	var e = {
		contacts: [{
			urls: []
		}]
	},
	t = $(".profile_name_sig").text().trim().split(",");
	e.contacts[0].name = t[0],
	t.length > 2 && (e.contacts[0].title = t[1]);
	var r = $(".sn_icon_stack a");
	return r.each(function () {
		var t = $(this).attr("href");
		e.contacts[0].urls.push({
			url: t,
			type: "url"
		})
	}),
	e
}
function handleAngelList() {
	var e = {
		name: $($(".name")[0]).text().trim(),
		description: $(".high_concept").text().trim(),
		addresses: [{}],
		contacts: [{
			urls: []
		}]
	},
	t = $(".location-tag").text().split(",");
	return 1 === t.length ? e.city = t[0].trim() : 2 === t.length && (e.city = t[0].trim(), e.state = t[1].trim()),
	$(".tags").next().find("a").each(function () {
		var t = $(this).attr("href");
		$(this).hasClass("website-link") ? e.url = t: e.contacts[0].urls.push({
			url: t,
			type: "url"
		})
	}),
	e
}
function handleElastic() {
	return {
		name: "Elastic Inc.",
		url: "http://elasticsales.com",
		description: "Elastic is AWS for sales",
		addresses: [{
			address_1: "1955 Landings Dr.",
			city: "Mountain View",
			state: "CA",
			zipcode: "94043",
			country: "US"
		}],
		contacts: [{
			phones: [{
				phone: "+1-818-635-6350",
				type: "office"
			}]
		}]
	}
}
function handleDefault() {
	var e, t = /((?:\+?1[\-. ]?)?\(?[0-9]{3}\)?[\-. ]?[0-9]{3}[\-. ]?[0-9]{4})+/gi,
	r = /(\+(?:[0-9] ?){6,14}[0-9])+/gi,
	n = $("body").clone(),
	a = [],
	i = [];
	n.find("script").remove(),
	n = n.text().trim(),
	a = n.match(t) || [],
	a = a.concat(n.match(r) || []);
	var s = [].concat(a);
	for (a = [], e = 0; s.length > e; e++) {
		var o = s[e].replace(/[\- ]/g, ""); - 1 === a.indexOf(o) && a.push(o)
	}
	var c = [],
	l = n.split(" ");
	for (e = 0; l.length > e; e++) {
		var d = l[e].trim(); - 1 !== d.indexOf("@") && c.push(d)
	}
	for (e = 0; c.length > e; e++) {
		var u = c[e];
		isRFC822ValidEmail(u) && -1 === i.indexOf(u.toLowerCase()) && i.push(u)
	}
	var p = {
		contacts: [],
		custom: {}
	},
	m = {
		emails: [],
		phones: []
	};
	for (e = 0; a.length > e; e++) m.phones.push({
		phone: a[e],
		type: "office"
	});
	for (e = 0; i.length > e; e++) m.emails.push({
		email: i[e],
		type: "office"
	});
	return p.contacts.push(m),
	p
}
function getLead(e) {
	var t = window.location.href.host(),
	r = null;
	try {
		switch (t) {
		case "www.facebook.com":
			r = handleFacebook();
			break;
		case "www.linkedin.com":
			r = handleLinkedIn();
			break;
		case "github.com":
			r = handleGitHub();
			break;
		case "www.yelp.com":
			r = handleYelp();
			break;
		case "www.crunchbase.com":
			r = handleCrunchBase();
			break;
		case "mail.google.com":
			r = handleGmail();
			break;
		case "www.jigsaw.com":
			r = handleJigsaw();
			break;
		case "www.quora.com":
			r = handleQuora();
			break;
		case "angel.co":
			r = handleAngelList();
			break;
		case "elasticsales.com":
			r = handleElastic();
			break;
		default:
			r = handleDefault()
		}
	} catch(n) {
		console.log(n);
	}
	r ? (r.url && 0 !== r.url.indexOf("http") && (r.url = "http://" + r.url), r.custom || (r.custom = {}), r.custom.original_url || (r.custom.original_url = window.location.href), e(r)) : e(null)
}
String.prototype.host = function () {
	var e = document.createElement("a");
	return e.href = this,
	e.hostname
},
String.prototype.path = function () {
	var e = document.createElement("a");
	return e.href = this,
	e.pathname
},
chrome.extension.onRequest.addListener(function (e, t, r) {
	"getLead" === e.type && getLead(r)
});

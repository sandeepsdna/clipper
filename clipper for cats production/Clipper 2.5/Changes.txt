Date: 03 - Aug - 2014
1. Added copy button to copy profile details to clipboard. Expected to work only with Chrome browser
-Himanshu

Date: 03 Aug 2014
1. copy is now automatic
2. profile url is now truncated at id=...& - rest of the parameters are not kept

Date: 03 Aug 2014
1. Copy even if the lead exists in the database
2. if exists, copy a field "Lead Exists", and if not then ..
3. added timestamp to say when the copying was done - a lead may not exists, but will do later



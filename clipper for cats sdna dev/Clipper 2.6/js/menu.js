$(document).ready(function () {
    
	$( "#expertiseSelect" ).change(function() {
		var exper = $( "#expertiseSelect" ).val();
		
		if( exper == 'Technical')
		{
			$( "#techexp" ).show();
			$( "#techskill" ).show();
			$( "#saledel" ).hide();
			$( "#saleskill" ).hide();
			$('#salesSelect').val('');
			$('#saleskill').val('');
			
		}
		else if(exper == 'Non-Technical')
		{
			$( "#techexp" ).hide();
			$( "#techskill" ).hide();
			$( "#saledel" ).show();
			$( "#saleskill" ).show();
			$('#technicalexperience').val('');
			$('#technicalskills').val('');
			
		}
		else
		{
			$( "#techexp" ).hide();
			$( "#techskill" ).hide();
			$( "#saledel" ).hide();
			$( "#saleskill" ).hide();
			$('#technicalexperience').val('');
			$('#technicalskills').val('');
			$('#salesSelect').val('');
			$('#saleskill').val('');
			
		}
		
		//alert( exper );
	});
	
	
	
	
	var dup_confirm = 0;
	function get_info() {
		var e = $("#url").val() || null;
		e && 0 !== e.indexOf("http") && (e = "http://" + e);
		var t = {
			//organization_id: $(".org-list").val(),
			name: $("#name").val() || null,
			url: e,
			description: $("#description").val() || null,
			addresses: [],
			contacts: [{
				name: $("#contact_name").val() || null,
				title: $("#contact_title").val() || null,
				company: $("#name").val() || null,
				skills: $("#skills").val() || null,
				expertiseSelect: $("#expertiseSelect").val() || null,
				technicalexperience: $("#technicalexperience").val() || null,
				technicalskills: $("#technicalskills").val() || null,
				salesSelect: $("#salesSelect").val() || null,
				profile_pic: $("#profile_pic").val() || null,
				update_confirm: dup_confirm,
				industry: $("#industry").val() || null,
				serviceLine: $("#serviceLine").val() || null,
				salary: $("#salary").val() || null,
				notes: $("#notes").val() || null,
				emails: [],
				phones: [],
				urls: []
			}],
			custom: {},
			creation_source: "clipper"
		};
		$(".emails input").each(function () {
			var e = $(this);
			e.val() && t.contacts[0].emails.push({
				email: e.val(),
				type: "office"
			})
		}),
		$(".phones input").each(function () {
			var e = $(this);
			e.val() && t.contacts[0].phones.push({
				phone: e.val(),
				type: "office"
			})
		}),
		$(".urls input").each(function () {
			var a = $(this);
			a.val() && (e = a.val(), 0 !== e.indexOf("http") && (e = "http://" + e), t.contacts[0].urls.push({
				url: e,
				type: "url"
			}))
		}),
		$(".custom-field").each(function () {
			var e = $(this).find(".custom-key").val(),
			a = $(this).find(".custom-value").val();
			e && a && (t.custom[e] = a)
		});
		var a = {};
		a.salary = $("#salary").val();
		a.notes = $("#notes").val();
		a.industry = $("#industry").val();
		return a.address_1 = $("#address_1").val(),
		a.address_2 = $("#address_2").val(),
		a.city = $("#city").val(),
		a.state = $("#state").val(),
		a.zipcode = $("#zipcode").val(),
		a.country = $("#country").val(),
		a.salary = $("#salary").val(),
		a.notes = $("#notes").val(),
		a.phones = $("#phones").val(),
		a.update_confirm = dup_confirm,
		(a.address_1 || a.address_2 || a.city || a.state || a.zipcode || a.country || a.salary || a.notes || a.industry || a.phones) && t.addresses.push(a),
		t
	}

	//$(document).on('click',"#copy", function(){
	copy_lead = function(optionalParams){
		var a = get_info();
		//chrome.extension.getBackgroundPage().console.log(a);
		var emailsText = "";
		for (i=0; i< a.contacts[0].emails.length; i++){
			if (a.contacts[0].emails[i].type=='office'){
				emailsText += a.contacts[0].emails[i].email+' ';
			}
		}
		var phonesText = "";
		for (i=0; i< a.contacts[0].phones.length; i++){
			if (a.contacts[0].phones[i].type=='office'){
				phonesText += a.contacts[0].phones[i].phone.replace('+','00').replace(' ','')+'; ';
			}
		}

		var profileURLRE  = new RegExp("(.*?&).*", "g");
		var profileUrl = profileURLRE.exec(a.custom.original_url)[1]; 
		profileUrl = profileUrl.substring(0, profileUrl.length - 1);
		
		
		
		// construct details array
		var details_to_copy = [a.contacts[0].name, a.contacts[0].title, a.name, a.addresses[0].address_1, profileUrl, emailsText, phonesText];
		for (i=0; i<optionalParams.length; i++){
			details_to_copy.push(optionalParams[i]);
		}
		// convert details array to csv
		var details_tsv="";
		for(i=0;i<details_to_copy.length; i++){
			//details_tsv += '"' + details_to_copy[i].replace('"','\\"') + '", ';
			details_tsv += details_to_copy[i] + '\t';
			//chrome.extension.getBackgroundPage().console.log(details_tsv);
		}
		//remove trailing comma
		//details_tsv = details_tsv.substring(0, details_tsv.length-1);
		//chrome.extension.getBackgroundPage().console.log(details_tsv);
		//copyFrom.text(details_tsv);
		var copyFrom = $('<textarea/>');
		copyFrom.text(details_tsv);
		$('body').append(copyFrom);
		copyFrom.select();
		document.execCommand('copy');
		copyFrom.remove();
		//alert('balls!');

	}

	var t, a, n = function () {
		var e = $(".org-list").val(),
		t = !1;
		if (a) {
			for (var n = 0; a.length > n; n++) if (a[n].organization_id === e) {
				$(".dupe-lead-url").attr("href", a[n].lead_url),
				$(".dupe-org-alert").slideDown(),
				t = !0;
				break
			}
			t || $(".dupe-org-alert").slideUp()
		}
	},
	s = function (e) {
		var t = (e.first_name || "") + " " + (e.last_name || "");
		$(".full-name").text(t);
		//if ($(".full-name").text(t), e.image && $(".profile-pic").empty().append($("<img>", {
		//	src: e.image,
		//	alt: ""
		//})))
		//else 
		$(".org-row").hide()
	},
	check_lead = function(){
	    var t = get_info();
	    chrome.extension.sendRequest({
		type: "check_lead_exists",
		lead: t
	    },
	    function (e) {
			if (!(e.success)) {
				var time = new Date();
				copy_lead(["Lead Exists", time.toString()]);
			    $(".overlay").addClass("post-update");
			    if (e.message === "Candidate exists"){
				$(".post-update").show();
				setTimeout(function () {
				    var e = -0.5 * $(".validation-errors").outerHeight() + "px";
				    $(".post-update").css("margin-top", e)
				    },
				10);
				$(".lead-url-check").attr("href", e.url)				
			    };
			}
			else{
				var time = new Date();
				copy_lead(["New Lead", time.toString()]);
			}
	    }
	)}
	show_lead = function (e, a) {
		var s;
		if (a) {
			if (a.url && 0 !== a.url.indexOf("http") && (a.url = "http://" + a.url), e.find("#name").val(a.name || ""), e.find("#url").val(a.url || ""), e.find("#description").val(a.description || ""), a.contacts && a.contacts.length) {
				$("#contact_name").val(a.contacts[0].name),
				$("#contact_title").val(a.contacts[0].title);
				var r = a.contacts[0].emails,
				i = a.contacts[0].phones,
				o = a.contacts[0].urls;
				if (r && r.length) for (t = 0; r.length > t; t++) s = $("<li>"),
				s.append($("<input>", {
					type: "text"
				}).val(r[t].email)),
				s.append($("<i>", {
					"class": "trash"
				})),
				e.find(".emails").append(s);
				if (i && i.length) for (t = 0; i.length > t; t++) s = $("<li>"),
				s.append($("<input>", {
					type: "text"
				}).val(i[t].phone)),
				s.append($("<i>", {
					"class": "trash"
				})),
				e.find(".phones").append(s);
				if (o && o.length) for (t = 0; o.length > t; t++) s = $("<li>"),
				s.append($("<input>", {
					type: "text"
				}).val(o[t].url)),
				s.append($("<i>", {
					"class": "trash"
				})),
				e.find(".urls").append(s)
			}
			if(a.skills)
				{
					//var new_skills = a.skills.join(" | ");
					e.find("#skills").val(a.skills);	
					e.find("tr.ant-skills").show();
				}
			else
				{
					e.find("tr.ant-skills").hide();
				}
			
			//e.find("#update_confirm").hide();
			
			if(a.profile_pic)
				{
					e.find("#profile_pic").val(a.profile_pic).show();	
				}
			else
				{
					e.find("#profile_pic").hide();
				}
			if(a.industry)
				{
					e.find("#industry").val(a.industry);
				}
			if(a.salary)
				{
					e.find("#salary").val(a.salary);	
				}
			if(a.notes)
				{
					e.find("#notes").val(a.notes);	
				}
			
			if (a.addresses && a.addresses.length) {
				var c = a.addresses[0];
				e.find("#address_1").val(c.address_1),
				e.find("#address_2").val(c.address_2),
				e.find("#city").val(c.city),
				e.find("#state").val(c.state),
				e.find("#zipcode").val(c.zipcode),
				e.find("#country").val(c.country)
			}

			$.each(a.custom, function (e, t) {
				var a = $("<input>", {
					"class": "custom-key",
					type: "text",
					placeholder: "Click to edit"
				}),
				n = $("<input>", {
					"class": "custom-value",
					type: "text",
					placeholder: "Click to edit"
				});
				a.val(e),
				n.val(t);
				var s = $("<td>").append(a),
				r = $("<td>").append(n);
				r.append($("<i>", {
					"class": "trash remove-custom"
				}));
				var i = $("<tr>", {
					"class": "custom-field"
				}).append(s).append(r);
				$(".add-custom").closest("tr").before(i)
			})
		}
		e.find(".emails").append($("<li>").append($("<a>", {
			"class": "add add-email"
		}).text("Add Email"))),
		e.find(".phones").append($("<li>").append($("<a>", {
			"class": "add add-phone"
		}).text("Add Phone"))),
		e.find(".urls").append($("<li>").append($("<a>", {
			"class": "add add-url"
		}).text("Add URL"))),
		n(),
		check_lead();
		copy_lead();
	}
	s_lead = function() {
		var t = get_info();
		$(".overlay").addClass("loading").show(),
		chrome.extension.sendRequest({
			type: "postLead",
			lead: t
		},
		function (e) {
			if (e.success) {
				$(".overlay").removeClass("loading"),
				$(".lead-url").attr("href", e.url),
				$(".post-success").show(),
				setTimeout(function () {
					$(".post-info").fadeOut(),
					$(".overlay").fadeOut(function () {
						window.close()
				    })
				},
			    3e3);
			}
			else {
				$(".overlay").removeClass("loading");
				
				if (e.message === "Candidate exists")
				{
				    $(".post-update").show();
					setTimeout(function () {
					    var e = -0.5 * $(".validation-errors").outerHeight() + "px";
					    $(".post-update").css("margin-top", e)
				        },
				        10);
					
					var s1 = $("<button>", {
						"class": "btn"
				    }).text("View It");
					//var s2 = $("<button>", {
					//	"class": "btn"
				    //}).text("Update");
					var s3 = $("<button>", {
						"class": "btn"
				    }).text("Go Back");
					
				    
				    $(".post-update").append(s1),
					//$(".post-update").append(s2),
					$(".post-update").append(s3),
				    s1.click(function () {
						$(".lead-url").attr("href", e.url),
					    $(".post-view").show(),
					    s1.remove(),
						//s2.remove(),
						s3.remove(),
					    dup_confirm = 0,
					    setTimeout(function () {
					        $(".post-info").fadeOut(),
					        $(".overlay").fadeOut(function () {
						    window.close()
				        })
				        },
			            3e3);
					}),
					
					
					//s2.click(function () {
					//setTimeout(function () {
					//        $(".post-info").fadeOut();
					//		$(".overlay").fadeOut(function () {
					//			s1.remove();
					//			s2.remove();
					//			s3.remove()
				    //    })
				    //    },
			        //    3e3);
					//$(".post-info").fadeOut();
					//dup_confirm = 1;
					//s_lead();
					//}),
					
					s3.click(function () {
				    $(".overlay").fadeOut(function () {
						s1.remove();
						//s2.remove();
						s3.remove();
					}),
					$(".post-info").fadeOut();
					dup_confirm = 0;
					})
				}
				else {
				    $(".post-error").show();
				    var s = $("<button>", {
						"class": "btn"
				    }).text("Go back");
					setTimeout(function () {
					var e = -0.5 * $(".validation-errors").outerHeight() + "px";
					$(".post-error").css("margin-top", e)
				    },
				    10);
				    
				    $(".post-error").append(s),
				    s.click(function () {
					$(".overlay").fadeOut(function () {
						s.remove()
					}),
					$(".post-info").fadeOut()
				    });
				}
								
				if ($(".validation-errors").html(""), 400 === e.xhr.status) {
					var t = $.parseJSON(e.xhr.responseText),
					a = t["field-errors"],
					n = "<br><br>";
					a.addresses && (n += "The country code has to be a 2-letter abbreviation."),
					a.phones && (n += "Some phone numbers are invalid."),
					a.url && (n += "The lead URL is invalid"),
					a.custom && (n += "Invalid custom key"),
					a.contacts && a.contacts.errors && (a.contacts.errors.emails && (n += "Some email addresses are invalid."), a.contacts.errors.urls && (n += "Some of the contact's URLs are invalid.")),
					n += "<br><br>",
					$(".validation-errors").html(n)
				}
			}
		})
	};
	chrome.extension.sendRequest({
		type: "getUserDetails"
	},
	function (e) {
		s(e.data)
	}),
	chrome.extension.sendRequest({
		type: "getCurrentLead"
	},
	function (e) {
		a = e.postedData,
		show_lead($(".lead-data"), e.lead)
	}),
	$(document).on("click", ".add", function () {
		var e, t = $(this);
		e = t.hasClass("add-email") ? "Email": t.hasClass("add-url") ? "URL": "Phone";
		var a = $("<li>"),
		n = $("<input>", {
			type: "text",
			placeholder: e
		});
		a.append(n),
		a.append($("<i>", {
			"class": "trash"
		})),
		t.parent().before(a),
		n.focus()
	}),
	$(document).on("click", ".add-custom", function (e) {
		e.preventDefault();
		var t = $("<td>").append($("<input>", {
			"class": "custom-key",
			type: "text",
			placeholder: "Click to edit"
		})),
		a = $("<td>").append($("<input>", {
			"class": "custom-value",
			type: "text",
			placeholder: "Click to edit"
		}));
		a.append($("<i>", {
			"class": "trash remove-custom"
		}));
		var n = $("<tr>", {
			"class": "custom-field"
		}).append(t).append(a);
		$(this).closest("tr").before(n)
	}),
	$(document).on("click", ".trash", function () {
		var e = $(this).parent();
		$(this).hasClass("remove-custom") && (e = e.parent()),
		e.remove()
	}),
	$(".save-lead").click(function () {
		
		var expert = $("#expertiseSelect").val();
		var industry = $("#industry").val();
		var serviceLine = $("#serviceLine").val();
		
		if( serviceLine == 'none' )
		{
			$("#error-serviceLineNew").show();
			$("#error-industryNew").hide();
			$("#error-expert").hide();
			$("#error-technical").hide();
			$("#error-skills").hide();
			$("#error-sales").hide();
				return false;
			
		}
		
		if( industry == 'none' )
		{
			$("#error-serviceLineNew").hide();
			$("#error-industryNew").show();
			$("#error-expert").hide();
			$("#error-technical").hide();
			$("#error-skills").hide();
			$("#error-sales").hide();
				return false;
			
		}
		
		if(expert == 'Technical')
		{
			
			var technicalexperience = $("#technicalexperience").val();
			var technicalskills = $("#technicalskills").val();
			if(technicalexperience == '')
			{
				$("#error-expert").hide();
				$("#error-skills").hide();
				$("#error-sales").hide();
				$("#error-industryNew").hide();
				$("#error-serviceLineNew").hide();
				$("#error-technical").show();
				return false;
			}
			if(technicalskills == '')
			{
				$("#error-expert").hide();
				$("#error-technical").hide();
				$("#error-sales").hide();
				$("#error-industryNew").hide();
				$("#error-serviceLineNew").hide();
				$("#error-skills").show();
				return false;
			}
	
		}
		if(expert == 'Non-Technical')
		{
			var salesSelect = $("#salesSelect").val();
			if(salesSelect == '')
			{
				$("#error-expert").hide();
				$("#error-skills").hide();
				$("#error-technical").hide();
				$("#error-industryNew").hide();
				$("#error-serviceLineNew").hide();
				$("#error-sales").show();
				return false;
			}
			
		}
		
		if(expert == 'none')
		{
			$("#error-expert").show();
			$("#error-industryNew").hide();
			$("#error-serviceLineNew").hide();
			return false;
		}
		else
		{
			s_lead()
			return true;
		}
		
		//,
		//dup_confirm ? sddress_1").val()_lead() : noop()
	}),
	$(".change-org").click(function () {
		$(this).toggleClass("active"),
		$(".orgs").slideToggle()
	}),
	$(".org-list").change(function () {
		var e = $(this).val();
		chrome.extension.sendRequest({
			type: "changeOrg",
			id: e
		}),
		$("body").focus(),
		n()
	}),
	$(".sign-out").click(function () {
		chrome.extension.sendRequest({
			type: "logout"
		},
		function () {
			window.close()
		})
	});
	var i = $("body"),
	o = setInterval(function () {
		i.focus(),
		i.is(":focus") && clearInterval(o)
	},
	300);
	$(document).on("keydown", "input", function (e) {
		13 === e.keyCode && $("body").focus()
	})
});

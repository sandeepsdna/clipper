$(document).ready(function () {
	var e = $("#user_name"),
	t = $("#user_password");
	$("#login_form").submit(function () {
		$("#loading").show(),
		$("#error").hide(),
		$("#login_form").hide(),
		$("#h1_text").hide();
		var a = e.val(),
		n = t.val();
		return chrome.extension.sendRequest({
			type: "login",
			username: a,
			password: n
		},
		function (e) {
			e.success ? ($("#loading").hide(), $("#success").show(), chrome.extension.sendRequest({
				type: "loadMenu"
			},
			function () {
				setTimeout(function () {
					window.close()
				},
				1e3)
			})) : ($("#loading").hide(), $("#login_form").show(), $("#h1_text").show(), $("#error").text(e.message), $("#error").show())
		}),
		!1
	});
	var a = setInterval(function () {
		e.focus(),
		e.is(":focus") && clearInterval(a)
	},
	300)
});